# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """
    InitState = problem.getStartState()
    if problem.isGoalState(InitState):
        return []  # Si estamos en el objetivo nada mas empezar ya esta

    frontera = util.Stack()  # Creamos una pila
    frontera.push((InitState, []))  # Cada elemento de la pila es el estado y el path.

    expanded = []  

    while not frontera.isEmpty():
        current_state, actions = frontera.pop()

        if problem.isGoalState(current_state):
            return actions  # Retorna la lista de acciones para llegar al objetivo


        #---Busqueda en profundidad---
        if current_state not in expanded:
            expanded.append(current_state)
            for next_state, action, cost in problem.getSuccessors(current_state):
                new_actions = actions + [action]
                frontera.push((next_state, new_actions))

    return []  # Retorna la lista vacia si hay fallo

    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()
    """


def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    InitState = problem.getStartState()
    if problem.isGoalState(InitState):
        return []  # Si estamos en el objetivo nada mas empezar ya esta

    frontera = util.Queue()  # Creamos una cola
    frontera.push((InitState, []))  # Cada elemento de la pila es el estado y el path.

    expanded = []

    while not frontera.isEmpty():
        current_state, actions = frontera.pop()

        if problem.isGoalState(current_state):
            return actions  # Retorna la lista de acciones para llegar al objetivo

        #---Busqueda en anchura---
        if current_state not in expanded:   
            expanded.append(current_state)
            for next_state, action, cost in problem.getSuccessors(current_state):
                new_actions = actions + [action]
                frontera.push((next_state, new_actions))

    return []  # Retorna la lista vacia si hay fallo


def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"

    InitState = problem.getStartState()
    if problem.isGoalState(InitState):
        return []  # Si estamos en el objetivo nada mas empezar ya esta

    frontera = util.PriorityQueue()  # Creamos una cola de prioridad
    frontera.push((InitState, []), 0)  # Cada elemento de la pila es el estado, el path y el cote de la accion.

    expanded = [] 

    while not frontera.isEmpty():
        current_state, actions = frontera.pop()

        if problem.isGoalState(current_state):
            return actions  # Retorna la lista de acciones para llegar al objetivo


        #---Busqueda de coste uniforme---
        if current_state not in expanded:
            expanded.append(current_state)
            for next_state, action, cost in problem.getSuccessors(current_state):
                new_actions = actions + [action]
                new_cost = problem.getCostOfActions(new_actions)
                frontera.push((next_state, new_actions), new_cost)

    return []  # Retorna la lista vacia si hay fallo

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    InitState = problem.getStartState()
    if problem.isGoalState(InitState):
        return []  # Si estamos en el objetivo nada mas empezar ya esta

    frontera = util.PriorityQueue()  # Creamos una cola de prioridad
    frontera.push((InitState, []), 0)  # Cada elemento de la pila es el estado, el path y el cote de la accion.

    expanded = [] 

    while not frontera.isEmpty():
        current_state, actions = frontera.pop()

        if problem.isGoalState(current_state):
            return actions  # Retorna la lista de acciones para llegar al objetivo

        #---Busqueda en A*---
        if current_state not in expanded:
            expanded.append(current_state)
            for next_state, action, cost in problem.getSuccessors(current_state):
                new_actions = actions + [action]
                new_cost = problem.getCostOfActions(new_actions) + heuristic(next_state, problem)
                frontera.push((next_state, new_actions), new_cost)

    return []  # Retorna la lista vacia si hay fallo


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
